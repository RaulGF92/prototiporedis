package es.prototipo.controller.dto.response;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class ResponseIsAlive {

	private Date timestamp;

	public ResponseIsAlive() {
		LocalDate today = LocalDate.now();
		LocalDateTime localDateTime = today.atStartOfDay();
		this.timestamp = Date.from( localDateTime.atZone( ZoneId.systemDefault()).toInstant());
	}
	
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
