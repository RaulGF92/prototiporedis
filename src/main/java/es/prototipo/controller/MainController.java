package es.prototipo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.prototipo.controller.dto.response.ResponseIsAlive;

@RestController
public class MainController {

    @RequestMapping(value = "/isAlive", produces = "application/json")
    public ResponseIsAlive isAlive() {
    	return new ResponseIsAlive();
    }
}
