package es.prototipo.controller;

import static org.junit.Assert.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.prototipo.controller.dto.response.ResponseIsAlive;

import static org.hamcrest.Matchers.containsString;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MainControllerTest {

	@Autowired
	private MainController controller;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Autowired
	ObjectMapper objectMapper;
	
	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testGetInfo() throws Exception {
		this.mockMvc.perform(get("/isAlive"))
			.andDo(print())
			.andExpect(content().string(containsString(this.generateResponseIsAlive())))
			.andExpect(status().isOk());
	}

	private String generateResponseIsAlive() throws JsonProcessingException {
		ResponseIsAlive response = new ResponseIsAlive();
		return objectMapper.writeValueAsString(response);
	}

	@Test
	public void contexLoads() throws Exception {
		assertNotNull(controller);
	}

}
